package texter

/**
 * Some embedded sample data.
 */
class SampleData {
    final static String TargetBoardOfDirectors =
"""

<!DOCTYPE html><html><!--###PHBoeHBhZ2U+PHRpbWVTdGFtcD4wNS8wNC8yMDE0IDEzOjQ4OjEzPC90aW1lU3RhbXA+PHRpbWVUaWxsQ0NCTlJlZnJlc2g+MTgwPC90aW1lVGlsbENDQk5SZWZyZXNoPjwvcGh4cGFnZT4=###--><head><link href="http://phx.corporate-ir.net/HttpCombiner.ashx?s=RisenCSS&amp;v=B94A18012C20431FA6ADC43CFC76EDB2" type="text/css" rel="stylesheet" /><title>Board of Directors | Target Corporation</title><link rel="shortcut icon" type="image/x-icon" href="http://www.target.com/favicon.ico" /><meta http-equiv="X-UA-Compatible" content="IE=Edge" /><meta name="description" content="Target Corporation is committed to good corporate governance practices - always has, always will. Download our documentation to see." /><meta name="keywords" content="" /><meta property="og:title" content="" /><meta property="og:description" content="" /><meta property="og:image" content="" /><link href="http://corporate.target.com/_ui/css/main.css" type="text/css" rel="stylesheet" media="all" /><link href="http://corporate.target.com/_ui/css/investors.css" type="text/css" rel="stylesheet" media="all" />
            <!--[if lte IE 8]><link href="client/65/65828/css/ie8.css" rel="stylesheet" type="text/css" media="all" /><![endif]-->
    <!--[if lte IE 7]><link href="client/65/65828/css/ie7.css" rel="stylesheet" type="text/css" media="all" /><![endif]-->
    <script src="client/65/65828/script/modernizr.custom.js"></script><script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script><script>window.jQuery || document.write('<script src="client/65/65828/script/jquery-1.7.2.min.js"><\\/script>')</script><script src="client/65/65828/script/lib-combined.js"></script><script src="client/65/65828/script/global-combined.js"></script><link rel="stylesheet" href="client/65/65828/css/font.css" /><link rel="stylesheet" href="client/65/65828/css/ccbnIR.css" /><script type="text/javascript" language="Javascript">
        ccbn_no_doctype=false;
    </script><script type="text/javascript">
\$(document).ready(function() {

\$('#download_help').hide();

\$('.investors_tooltip_link').mouseover(function() {

\$('#download_help').toggle();

// return false so any link destination is not followed
return false;
\t\t\t\t});
\t\t\t\t
});

</script>
<!--[if IE 8]>
<style>
#secSubmit  {float: right;}
.secSubmit td {vertical-align: middle !important;}
.ccbnBgInputValign * {vertical-align: middle !important;}
</style>
<![endif]-->
<script>
                var _gaq = _gaq || [];
                (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
                var pop = document.createElement('script'); pop.type = 'text/javascript'; pop.async = true;
                pop.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'corporate.target.com/_ui/js/ga/global/popanalytics-global.js';
                var js = document.getElementsByTagName('script')[0]; js.parentNode.insertBefore(pop, js);
                })();
                </script><script>
\$(document).ready(function(){
    \$("img[src='http://media.corporate-ir.net/media_files/irol/global_images/spacer.gif']").attr("alt","");
    \$("a[target='_blank']").attr("title","Link opens in a new window");
});
</script><script src="http://phx.corporate-ir.net/HttpCombiner.ashx?s=RisenJS&v=B94A18012C20431FA6ADC43CFC76EDB2" type="text/javascript"></script><script type="text/javascript">Phx.AjaxToken = 'b11f528329bbcb978c6263efe73939aa2452db019b4dead38bbb2a9ddfa7cb25';</script><script type="text/javascript">var s_CCSWebHostingAccount = "trcgclientweb1915";</script><script type="text/javascript" src="./WebSideStory/s_code.js"></script></head><body class="no-js investors"><script>  !function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = "https://platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs); } } (document, "script", "twitter-wjs");</script><noscript><h2><center>This website requires Javascript.</center></h2></noscript><nav role="navigation"><ul class="skip-links"><li><a href="#nav-main" class="navJump accessLabel">Skip to main navigation</a></li><li><a href="#main-content" class="accessLabel">Skip to content</a></li><li><a href="#global-footer" class="accessLabel">Skip to footer</a></li></ul></nav><div id="wrapper" class="investors"><header id="global-header" role="banner"><div class="inner header" id="top"><div class="logo"><a href="http://corporate.target.com/"><img src="http://corporate.target.com/_ui/img/global/target-bullseye.png" alt="Target Corporation" /></a></div><div class="nav-primary redBtn" id="menuBtn"><a href="#menu" tabIndex="-1"><span class="offscreen">Menu</span></a></div><form role="search" action="http://corporate.target.com/search-results" method="get"><label for="search" id="searchLabel" class="redBtn"><span class="offscreen">Search:</span></label><div id="searchFields"><div><input type="search" id="search" name="q" placeholder="can we help you find something?" /><input type="hidden" id="start" name="start" value="0" /><input type="hidden" id="num" name="num" value="10" /><input type="submit" value="go" class="submit redBtn" /></div></div></form><nav id="nav-main" role="navigation"><ul id="nav-primary"><li id="nav-home"><a href="http://corporate.target.com/" class="nav-item-primary" tabIndex="-1">home</a></li><li id="nav-about"><a href="http://corporate.target.com/about/" class="nav-item-primary"><span class="defaultText">about Target</span><span class="updateLabel"></span><span class="accessLabel"> expand</span></a><nav class="nav-secondary"><span class="indicator"></span><div class="subnav-wrapper"><ul><li><a href="http://corporate.target.com/about/">about Target home</a></li><li><a href="http://corporate.target.com/about/mission-values/">mission &amp; values</a></li><li><a href="http://corporate.target.com/about/shopping-experience/">the shopping experience</a></li><li><a href="http://corporate.target.com/about/design-innovation/">design &amp; innovation</a></li></ul><ul><li><a href="http://corporate.target.com/about/history/">history</a></li><li><a href="http://corporate.target.com/about/awards-recognition/">awards &amp; recognition</a></li><li><a href="http://corporate.target.com/about/suppliers/">suppliers</a></li></ul><div class="clear"></div></div><span class="img"></span></nav></li><li id="nav-careers"><a href="http://corporate.target.com/careers/" class="nav-item-primary"><span class="defaultText">careers</span><span class="updateLabel"></span><span class="accessLabel"> expand</span></a><nav class="nav-secondary"><span class="indicator"></span><div class="subnav-wrapper"><ul><li><a href="http://corporate.target.com/careers/">careers home</a></li><li><a href="http://corporate.target.com/careers/career-areas/">career areas</a></li><li><a href="http://corporate.target.com/careers/culture/">culture</a></li><li><a href="http://corporate.target.com/careers/benefits/">benefits</a></li><li><a href="http://corporate.target.com/careers/global-locations/">global locations</a></li><li><a href="http://corporate.target.com/careers/recruiting-events/">recruiting events</a></li><li><a href="http://corporate.target.com/careers/faqs/">careers FAQs</a></li></ul><ul><li><strong>explore by background:</strong></li><li><a href="http://corporate.target.com/careers/college-students/">college students</a></li><li><a href="http://corporate.target.com/careers/mbas-graduate-degrees/">MBAs &amp; graduates</a></li><li><a href="https://corporate.target.com/careers/healthcare">healthcare</a></li></ul><div class="clear"></div></div><span class="img"></span></nav></li><li id="nav-corporate-responsibility"><a href="http://corporate.target.com/corporate-responsibility/" class="nav-item-primary"><span class="defaultText">corporate responsibility</span><span class="updateLabel"></span><span class="accessLabel"> expand</span></a><nav class="nav-secondary"><span class="indicator"></span><div class="subnav-wrapper"><ul><li><a href="http://corporate.target.com/corporate-responsibility/">corporate responsibility home</a></li><li><a href="http://corporate.target.com/corporate-responsibility/ceo-message/">CEO message</a></li><li><a href="http://corporate.target.com/corporate-responsibility/grants/">grants</a></li><li><a href="http://corporate.target.com/corporate-responsibility/community-events/">community events</a></li><li><a href="http://corporate.target.com/corporate-responsibility/goals-reporting/">goals &amp; reporting</a></li><li><a href="http://corporate.target.com/corporate-responsibility/stakeholder-engagement/">stakeholder engagement</a></li><li><a href="http://corporate.target.com/corporate-responsibility/civic-activity/">civic activity</a></li></ul><ul><li class="nested"><strong>areas of commitment:</strong><ul><li><a href="http://corporate.target.com/corporate-responsibility/education/">education</a></li><li><a href="http://corporate.target.com/corporate-responsibility/environment/">environment</a></li><li><a href="http://corporate.target.com/corporate-responsibility/health-well-being/">health &amp; well-being</a></li><li><a href="http://corporate.target.com/corporate-responsibility/team-members/">team members</a></li><li><a href="http://corporate.target.com/corporate-responsibility/responsible-sourcing/">responsible sourcing</a></li><li><a href="http://corporate.target.com/corporate-responsibility/safety-preparedness/">safety &amp; preparedness</a></li><li><a href="http://corporate.target.com/corporate-responsibility/volunteerism/">volunteerism</a></li></ul></li></ul><div class="clear"></div></div><span class="img"></span></nav></li><li id="nav-investors"><a href="#" class="nav-item-primary current"><span class="defaultText">investors</span><span class="updateLabel"></span><span class="accessLabel"> expand</span></a><nav class="nav-secondary"><span class="indicator"></span><div class="subnav-wrapper"><ul><li><a href="phoenix.zhtml?c=65828&amp;p=irol-irhome">investors home</a></li><li><a href="phoenix.zhtml?c=65828&amp;p=irol-homeProfile">corporate overview</a></li><li><a href="phoenix.zhtml?c=65828&amp;p=irol-stockQuoteChart">stock information</a></li><li><a href="phoenix.zhtml?c=65828&amp;p=irol-reportsAnnual">annual reports</a></li><li><a href="phoenix.zhtml?c=65828&amp;p=irol-sec">sec filings</a></li><li><a href="phoenix.zhtml?c=65828&amp;p=irol-estimates">analyst coverage</a></li></ul><ul><li><a href="phoenix.zhtml?c=65828&amp;p=irol-news&amp;nyo=0">financial news</a></li><li><a href="phoenix.zhtml?c=65828&amp;p=irol-summaryFinancial">summary financials</a></li><li><a href="phoenix.zhtml?c=65828&amp;p=irol-calendar">events &amp; presentations</a></li><li><a href="phoenix.zhtml?c=65828&amp;p=irol-govHighlights">corporate governance</a></li><li><a href="phoenix.zhtml?c=65828&amp;p=irol-stockPurchase">shareholder services</a></li></ul><div class="clear"></div></div><span class="img"></span></nav></li><li id="nav-press" class="last"><a href="http://pressroom.target.com/" class="nav-item-primary"><span class="defaultText">press</span><span class="updateLabel"></span><span class="accessLabel"> expand</span></a><nav class="nav-secondary"><span class="indicator"></span><div class="subnav-wrapper"><ul><li><a href="http://pressroom.target.com/">press home</a></li><li><a href="http://pressroom.target.com/corporate">corporate fact sheet</a></li><li><a href="http://pressroom.target.com/leadership">leadership</a></li><li><a href="http://pressroom.target.com/news">releases</a></li></ul><ul><li><a href="http://pressroom.target.com/multimedia">multimedia library</a></li><li><a href="http://pressroom.target.com/faq">media FAQs</a></li><li><a href="http://pressroom.target.com/presskit">press kits</a></li></ul><div class="clear"></div></div><span class="img"></span></nav></li></ul><ul id="nav-utility">
        <li><a href="http://www.target.com/">shop Target.com</a></li>
        <li><a href="http://www.target.com/store-locator/find-stores">find a store</a></li>
        <li><a href="http://www.target.com/HelpContent?help=/sites/html/TargetOnline/help/contact_us/contact_us.html">contact us</a></li></ul></nav></div></header><div id="main-content"><div class="inner"><nav class="breadcrumb"><ul><li><a href="http://corporate.target.com">home</a></li><li><a href="phoenix.zhtml?c=65828&amp;p=irol-irhome">investors</a></li><li><a href="phoenix.zhtml?c=65828&amp;p=irol-govhighlights">corporate governance</a></li><li><a class="current" href="javascript:void(0);" onClick="window.location.href=window.location.href">Board of Directors</a></li></ul></nav></div><section class="hero-wrapper"><div class="inner"><div class="hero"><h1>Board of Directors</h1><div class="ccbnWrapper" style="padding-bottom: 120px; min-height: 600px"><div><p class="text">The duty of the board of directors of Target Corporation is to act on behalf of shareholders and oversee management. The company believes there is a direct correlation between the quality of the company's board of directors and the overall performance of the corporation.</p><table width="100%" border="0" cellpadding="0" cellspacing="0" class="noPad"><tr class="noPad"><td align="left" nowrap="nowrap"><span class="pdf"><a href="http://media.corporate-ir.net/media_files/irol/65/65828/corpgov/BoardofDirectorsPD1.pdf" target="_blank" class="gray">Board of Directors Position Description</a><span class="offscreen"> opens in a new window</span></span> | 93KB</td></tr></table><br /><div id="pageWrap"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><table width="100%" border="0" cellspacing="1" cellpadding="3"><tr class="ccbnBgTblTxt"><td width="100%" valign="top"><span class="ccbnTxtBold">Roxanne&#160;S.&#160;Austin</span><br /><span class="ccbnTxtBold">President, Austin Investment Advisors</span></td></tr><tr class="ccbnBgTblTxt"><td valign="top"><span class="ccbnTxt">Roxanne S. Austin is President of Austin Investment Advisors, a private investment and consulting firm, a position she has held since January 2004.  From June 2009 until July 2010, she also served as President, Chief Executive Officer and a director of Move Networks, Inc., an Internet television services provider.</span></td></tr></table><br /><table width="100%" border="0" cellspacing="1" cellpadding="3"><tr class="ccbnBgTblTxt"><td width="100%" valign="top"><span class="ccbnTxtBold">Douglas&#160;M.&#160;Baker, Jr.</span><br /><span class="ccbnTxtBold">Chairman and Chief Executive Officer, Ecolab Inc.</span></td></tr><tr class="ccbnBgTblTxt"><td valign="top"><span class="ccbnTxt">Douglas M. Baker, Jr., is Chairman and Chief Executive Officer of Ecolab Inc., a provider of water and hygiene services and technologies for the food, hospitality, industrial and energy markets. He has served as Chairman of the Board of Ecolab since May 2006 and Chief Executive Officer since July 2004, and served as President from 2002 to 2011.</span></td></tr></table><br /><table width="100%" border="0" cellspacing="1" cellpadding="3"><tr class="ccbnBgTblTxt"><td width="100%" valign="top"><span class="ccbnTxtBold">Calvin&#160;Darden</span><br /><span class="ccbnTxtBold">Chairman, Darden Development Group</span></td></tr><tr class="ccbnBgTblTxt"><td valign="top"><span class="ccbnTxt">Calvin Darden is Chairman of Darden Development Group, LLC, a real estate development company, a position he has held on a full-time basis since November 2009. From February 2006 to November 2009, he was Chairman of The Atlanta Beltline, Inc., an urban revitalization project for the City of Atlanta.</span></td></tr></table><br /><table width="100%" border="0" cellspacing="1" cellpadding="3"><tr class="ccbnBgTblTxt"><td width="100%" valign="top"><span class="ccbnTxtBold">Henrique&#160;De Castro</span><br /><span class="ccbnTxtBold">Former Chief Operating Officer, Yahoo! Inc.</span></td></tr><tr class="ccbnBgTblTxt"><td valign="top"><span class="ccbnTxt">Henrique De Castro is the former Chief Operating Officer of Yahoo! Inc., a digital media company that delivers personalized digital content and experiences worldwide by offering online properties and services to users. He  held that position from November 2012 to January 2014. He previously served Google Inc. as President, Partner Business Worldwide from March 2012 to November 2012, President, Global Media, Mobile & Platforms from June 2009 to March 2012, and as Managing Director, European Sales from July 2006 to May 2009.</span></td></tr></table><br /><table width="100%" border="0" cellspacing="1" cellpadding="3"><tr class="ccbnBgTblTxt"><td width="100%" valign="top"><span class="ccbnTxtBold">James&#160;A.&#160;Johnson</span><br /><span class="ccbnTxtBold">Founder and Principal, Johnson Capital Partners</span></td></tr><tr class="ccbnBgTblTxt"><td valign="top"><span class="ccbnTxt">James A. Johnson founded Johnson Capital Partners, a private consulting company, in January 2000 and he continues to be actively engaged with that firm.  Mr. Johnson was Chairman of Perseus, LLC, a merchant banking private equity firm, from April 2001 to June 2012.</span></td></tr></table><br /><table width="100%" border="0" cellspacing="1" cellpadding="3"><tr class="ccbnBgTblTxt"><td width="100%" valign="top"><span class="ccbnTxtBold">Mary&#160;E.&#160;Minnick</span><br /><span class="ccbnTxtBold">Partner, Lion Capital LLP</span></td></tr><tr class="ccbnBgTblTxt"><td valign="top"><span class="ccbnTxt">Mary E. Minnick is a Partner of Lion Capital, a consumer-focused private investment firm, a position she has held since May 2007.</span></td></tr></table><br /><table width="100%" border="0" cellspacing="1" cellpadding="3"><tr class="ccbnBgTblTxt"><td width="100%" valign="top"><span class="ccbnTxtBold">Anne&#160;M.&#160;Mulcahy</span><br /><span class="ccbnTxtBold">Chairman of the Board of Trustees, Save The Children Federation, Inc.</span></td></tr><tr class="ccbnBgTblTxt"><td valign="top"><span class="ccbnTxt">Anne M. Mulcahy is Chairman of the Board of Trustees of Save The Children Federation, Inc., a non-profit organization dedicated to creating lasting change in the lives of children throughout the world, a position she has held since March 2010. She previously served as Chairman of the Board of Xerox Corp., a document management company, from January 2002 to May 2010, and Chief Executive Officer of Xerox from August 2001 to July 2009.</span></td></tr></table><br /><table width="100%" border="0" cellspacing="1" cellpadding="3"><tr class="ccbnBgTblTxt"><td width="100%" valign="top"><span class="ccbnTxtBold">Derica&#160;W.&#160;Rice</span><br /><span class="ccbnTxtBold">Executive Vice President, Global Services and Chief Financial Officer, Eli Lilly and Company</span></td></tr><tr class="ccbnBgTblTxt"><td valign="top"><span class="ccbnTxt">Derica W. Rice is Executive Vice President, Global Services and Chief Financial Officer of Eli Lilly and Company, a pharmaceutical company, positions he has held since January 2010 and May 2006, respectively. From May 2006 to December 2009, he served as Eli Lilly’s Senior Vice President and Chief Financial Officer.</span></td></tr></table><br /><table width="100%" border="0" cellspacing="1" cellpadding="3"><tr class="ccbnBgTblTxt"><td width="100%" valign="top"><span class="ccbnTxtBold">Kenneth&#160;L.&#160;Salazar</span><br /><span class="ccbnTxtBold">Partner, WilmerHale</span></td></tr><tr class="ccbnBgTblTxt"><td valign="top"><span class="ccbnTxt">Kenneth L. Salazar is a Partner at WilmerHale, a business and commercial legal services law firm, a position he has held since June 2013.  Previously, Mr. Salazar served as the U.S. Secretary of the Interior from 2009 to 2013; and served as a U.S. Senator from Colorado from 2005 to 2009.</span></td></tr></table><br /><table width="100%" border="0" cellspacing="1" cellpadding="3"><tr class="ccbnBgTblTxt"><td width="100%" valign="top"><span class="ccbnTxtBold">Gregg&#160;W.&#160;Steinhafel</span><br /><span class="ccbnTxtBold">Chairman of the Board, Chief Executive Officer and President, Target Corporation</span></td></tr><tr class="ccbnBgTblTxt"><td valign="top"><span class="ccbnTxt">Gregg W. Steinhafel is Chairman of the Board, Chief Executive Officer and President of Target. He began his career at the company as a merchandise trainee in 1979 and subsequently advanced through a variety of merchandising and operational management positions. Mr. Steinhafel became President of Target in August 1999, Chief Executive Officer in May 2008 and Chairman in February 2009.</span></td></tr></table><br /><table width="100%" border="0" cellspacing="1" cellpadding="3"><tr class="ccbnBgTblTxt"><td width="100%" valign="top"><span class="ccbnTxtBold">John&#160;G. Stumpf</span><br /><span class="ccbnTxtBold">Chairman of the Board, President and Chief Executive Officer, Wells Fargo & Company</span></td></tr><tr class="ccbnBgTblTxt"><td valign="top"><span class="ccbnTxt">John G. Stumpf is Chairman of the Board, President and Chief Executive Officer of Wells Fargo & Company, a banking and financial services company. He has been President since August 2005, Chief Executive Officer since June 2007, and became Chairman in January 2010. A 30-year veteran of Wells Fargo, he has held various operational and managerial positions throughout his career.</span></td></tr></table><br /><table width="100%" border="0" cellspacing="1" cellpadding="3"><tr class="ccbnBgTblTxt"><td width="100%" valign="top"><span class="ccbnTxtBold">Solomon&#160;D.&#160;Trujillo</span><br /><span class="ccbnTxtBold">Former Chief Executive Officer, Telstra Corporation Limited</span></td></tr><tr class="ccbnBgTblTxt"><td valign="top"><span class="ccbnTxt">Solomon D. Trujillo served as Chief Executive Officer and a director of Telstra Corporation Limited, Australia’s leading telecommunications company, from July 2005 to May 2009.</span></td></tr></table><br /></td><td><img src="http://media.corporate-ir.net/media_files/irol/global_images/spacer.gif" width="10" height="1" /></td></tr></table></div></div></div></div><aside class="sidebar"><div class="stockInformation arrows sidebar-section"><h3>corporate governance</h3><ul><li><a href="http://phx.corporate-ir.net/External.File?item=UGFyZW50SUQ9MTc1Nzc1fENoaWxkSUQ9LTF8VHlwZT0z&amp;t=1">Governance Guidelines <img src="http://media.corporate-ir.net/media_files/IROL/65/65828/redesign2012/img/sections/investors/stock-information/red_sidebar_arrow.png" alt="" /></a></li><li><a href="phoenix.zhtml?c=65828&amp;p=irol-govManage">Executive Officers <img src="http://media.corporate-ir.net/media_files/IROL/65/65828/redesign2012/img/sections/investors/stock-information/red_sidebar_arrow.png" alt="" /></a></li><li><a href="phoenix.zhtml?c=65828&amp;p=irol-govBoard">Board of Directors <img src="http://media.corporate-ir.net/media_files/IROL/65/65828/redesign2012/img/sections/investors/stock-information/red_sidebar_arrow.png" alt="" /></a></li><li><a href="phoenix.zhtml?c=65828&amp;p=irol-govcommittees">Board Committees <img src="http://media.corporate-ir.net/media_files/IROL/65/65828/redesign2012/img/sections/investors/stock-information/red_sidebar_arrow.png" alt="" /></a></li><li><a href="phoenix.zhtml?c=65828&amp;p=irol-govconduct">Business Conduct Guide <img src="http://media.corporate-ir.net/media_files/IROL/65/65828/redesign2012/img/sections/investors/stock-information/red_sidebar_arrow.png" alt="" /></a></li></ul></div></aside><aside class="sidebar"><div class="investorEmailAlerts sidebar-section first"><h3>investor email alerts</h3><p>To sign up for new email alerts, or to change your request, enter your email address below and click "go."</p><table width="100%" border="0" cellspacing="1" cellpadding="3" ><form name="Alerts"  Method="Post" Action="phoenix.zhtml?c=65828&p=irol-alertsLong&t=AlertPost" >

      <INPUT TYPE="Hidden" Name="ErrorLanguage" Value="">


      <INPUT TYPE="Hidden" Name="control_CaptchaModuleType" Value="Alerts">

      <INPUT TYPE="Hidden" Name="basepage" value="irol-alerts" ><tr class="modBgError"><td valign="top" class="ccbnError"><span class="ccbnError">\t\t
\t\t\t<script language="JavaScript">
\t\t\t<!--
\t\t\tfunction financial_news_warning(f, news_label, finnews_label)
\t\t\t{
\t\t\tif (news_label.Length==0) news_label="News Alerts";
\t\t\tif (finnews_label.Length==0) finnews_label="Financial News Alerts";
\t\t\tvar msg = 'You are attempting to sign up for both ' + news_label + ' and ' + finnews_label + ' for this company.  As part of the ' + news_label + ' subscription, you will receive ' + finnews_label + ' alerts by default.' ;
\t\t\t\t\t\t
\t\t\tvar news_is_on = false;
\t\t\tvar finnews_is_on = false;
\t\t\t
\t\t\tvar finnewsOn_index
\t\t\tvar finnewsOff_index\t
\t\t\t
\t\t\t
\t\t\t
\t\t\t// Loop through the items collection to see if both alert types are on.  We can't use the variable name directly because it contains a hypen and jscript equates that to a minus sign
\t\t\ti=0
\t\t\tdo
\t\t\t{
\t\t\t\tif (f.item(i).name=="ir-news"||f.item(i).name=="ir-financialnews")
\t\t\t\t{
\t\t\t\t\tif (f.item(i).checked)
\t\t\t\t\t{
\t\t\t\t\t\t if (f.item(i).value=="ir-news") news_is_on = true;
\t\t\t\t\t\t if (f.item(i).value=="ir-financialnews")
\t\t\t\t\t\t {
\t\t\t\t\t\t\tfinnews_is_on = true;
\t\t\t\t\t\t\tfinnewsOn_index = i ;
\t\t\t\t\t\t }
\t\t\t\t\t\t if (f.item(i).name=="ir-financialnews" && f.item(i).value=="")
\t\t\t\t\t\t {
\t\t\t\t\t\t\tfinnewsOff_index = i ;
\t\t\t\t\t\t }\t\t\t\t\t\t
\t\t\t\t\t}\t\t
\t\t\t\t\telse
\t\t\t\t\t{
\t\t\t\t\t\tif (f.item(i).name=="ir-financialnews" && f.item(i).value=="")
\t\t\t\t\t\t {
\t\t\t\t\t\t\tfinnewsOff_index = i ;
\t\t\t\t\t\t }
\t\t\t\t\t}\t
\t\t\t\t}
\t\t\t\ti++
\t\t\t} while (f.item(i)!=null)
\t\t\t
\t\t\tif (news_is_on&&finnews_is_on)
\t\t\t{
\t\t\t\talert(msg);
\t\t\t\tf.item(finnewsOn_index).checked = false ;
\t\t\t\tf.item(finnewsOff_index).checked = true ;
\t\t\t}
\t\t\t}
\t\t\t
\t\t\tfunction ccbnShowHideAlert(which,toggle)
\t\t\t{
\t\t\t\tccbnGetObj(which).style.display=(toggle==0)?'block':'none';
\t\t\t}
\t\t\t\t\t\t
\t\t\t//-->
\t\t\t</script>
\t\t</span></td></tr><tr class="ccbnBgInput"><td valign="top"><label class="offscreen" for="email">enter your email address</label><span class="investorEmailAlertsEmailInput">\t
\t\t\t<INPUT TYPE="TEXT" Name="control_AlertEmail" id="control_AlertEmail" value="">
\t\t</span>                     <input type="submit" value="go" class="redBtn" /></td></tr></form></table><script>
Phx.\$(document).ready( function() {
 Phx.\$('input[name=control_AlertEmail]').attr("id","email");  // set id
 Phx.\$('input[name=control_AlertEmail]').val('enter your email address')  // set default value

    // onfocus action
.focus(function(){
    if(\$(this).val() == "enter your email address"){
        \$(this).val("");
    }
    // focus lost action
}).blur(function(){
    if(\$(this).val() == ""){
        \$(this).val("enter your email address");
    }
});\t\t
});

</script></div><div class="hr-wrapper bg"><div class="hr"><hr /></div></div><div class="investorsContacts sidebar-section"><h3>investor contacts</h3><p><strong>investor relations</strong></p><p>
\t\tTarget Corporation<br />
\t\t1000 Nicollet Mall<br />
\t\tMinneapolis, MN 55403
\t</p><p><strong>p:</strong> (800) 775-3110<br /><strong>e:</strong> <a href="mailto:investorrelations@target.com">investorrelations@target.com</a></p><p><strong>transfer agent contact information</strong></p><p>
\t\tComputershare<br />
\t\tP.O. BOX 30170<br />
\t\tCollege Station, TX 77842-3170\t</p><p><strong>p:</strong> (800) 794-9871<br /><strong>e:</strong> <a href="https://www-us.computershare.com/investor/Contact">www-us.computershare.com/...</a></p><p><strong>w:</strong> <a href="https://www-us.computershare.com/Investor?cc=US" target="_blank" title="Link opens in a new window">www.computershare.com/investor<span class="offscreen"> Opens in a new window</span></a></p><p>Be sure to include your name, address, daytime phone number, email address and a reference to Target on all of your correspondence.</p></div><div class="hr-wrapper bg"><div class="hr"><hr /></div></div><div class="learnMoreAboutTarget sidebar-section bottomMargin"><h3>learn more about Target</h3><p>Here are a few more areas we suggest you explore: </p><ul><li><a href="http://corporate.target.com/corporate-responsibility/">Corporate Responsibility</a></li><li><a href="phoenix.zhtml?c=65828&amp;p=irol-govBoard">board of directors</a></li><li><a href="http://pressroom.target.com/leadership">executive officers</a></li><li><a href="http://corporate.target.com/about/shopping-experience/">the shopping experience</a></li><li><a href="http://corporate.target.com/corporate-responsibility/team-members">responsible employer</a></li><li><a href="http://corporate.target.com/about/history/">history</a></li></ul></div></aside><div class="clear"></div></div></section></div><footer id="global-footer" role="banner"><div class="inner"><div class="share-this"><h3>share this page</h3><ul><li><a href="http://www.facebook.com/sharer.php?u=http%3A%2F%2Finvestors.target.com%2Fphoenix.zhtml%3Fc%3D65828%26p%3Dirol-IRHome" class="facebook" title="Share this page on Facebook: Opens in a popup window"><span class="offscreen">share this page on facebook</span></a></li><li><a href="http://twitter.com/intent/tweet?url=http%3A%2F%2Finvestors.target.com%2Fphoenix.zhtml%3Fc%3D65828%26p%3Dirol-IRHome" class="twitter" title="Share this page on Twitter: Opens in a popup window"><span class="offscreen">share this page on twitter</span></a></li><li><a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http%3A%2F%2Finvestors.target.com%2Fphoenix.zhtml%3Fc%3D65828%26p%3Dirol-IRHome" class="linkedin" title="Share this page on LinkedIn: Opens in a popup window"><span class="offscreen">share this page on linkedIn</span></a></li></ul></div></div><div class="footer-inner"><nav><h3 class="offscreen">Additional Site Navigation</h3>
                    <ul id="footer-nav">

                        <li><a href="http://corporate.target.com/about/">about Target</a></li>
                        <li><a href="http://corporate.target.com/careers/">careers</a></li>
                        <li><a href="http://corporate.target.com/corporate-responsibility/">corporate responsibility</a></li>
                        <li><a href="phoenix.zhtml?c=65828&amp;p=irol-irhome">investors</a></li>
                        <li class="last"><a href="http://pressroom.target.com/">press</a></li>

                    </ul>
                    <ul id="footer-utility">
                        <li><a href="http://www.target.com">shop Target.com</a></li>
                        <li><a href="http://www.target.com/store-locator/find-stores">find a store</a></li>
                        <li><a href="http://www.target.com/HelpContent?help=/sites/html/TargetOnline/help/contact_us/contact_us.html">contact us</a></li>
                        <li><a href="http://www.target.com/spot/terms-conditions">terms &amp; conditions</a></li>

                        <li><a href="http://www.target.com/spot/privacy-policy">privacy policy</a></li>
                        <li><a href="http://www.target.com/spot/team-services">team member services</a></li>
                        <li>follow us on <a href="http://facebook.com/target" target="_blank" title="Link opens in a new window">facebook<span class="offscreen"> Opens in a new window</span></a> | <a href="http://twitter.com/target" target="_blank" title="Link opens in a new window">twitter<span class="offscreen"> Opens in a new window</span></a></li>

                    </ul><p class="copyright">© 2014 Target.com. All rights reserved. The Bullseye Design and Target are registered trademarks of Target Brands, Inc.</p></nav></div></footer></div><script src="client/65/65828/script/ccbn.js"></script></body></html>
"""

    final static String TargetLeadership =
"""


 <!DOCTYPE html>
<html lang="en">
<head>

    <title>Leadership: Meet the Executives |
  Target Corporate</title>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="Target executives help to guide and shape the future of Target.">
    <meta property="og:title" content="Leadership: Meet the Executives |
  Target Corporate" />
    <meta property="og:description" content="Target executives help to guide and shape the future of Target." />
    <link href="/media/themes/50057774fc96aa16b90045ad/images/favicon.ico" type="image/x-icon" rel="shortcut icon"/>
    <link href="/media/themes/50057774fc96aa16b90045ad/images/favicon.ico" type="image/x-icon" rel="icon"/>
    <link rel="alternate" type="application/rss+xml" title="Pressroom" href="http://pressroom.target.com/rss.xml">

    <link href="/media/themes/50057774fc96aa16b90045ad/css/press2.css" type="text/css" rel="stylesheet" media="all" />
    <link href="/media/themes/50057774fc96aa16b90045ad/css/main2.css" type="text/css" rel="stylesheet" media="all" />
    <link href="/media/themes/50057774fc96aa16b90045ad/css/media-queries2.css" type="text/css" rel="stylesheet" media="screen" />
    <link href="/media/themes/50057774fc96aa16b90045ad/css/style2.css" type="text/css" rel="stylesheet" />
    <link href="/media/themes/50057774fc96aa16b90045ad/css/colorbox_1.3.css" type="text/css" rel="stylesheet" />
    <!--[if lte IE 8]><link href="/media/themes/50057774fc96aa16b90045ad/css/ie8.css" rel="stylesheet" type="text/css" media="all" /><![endif]-->
    <!--[if lte IE 7]><link href="/media/themes/50057774fc96aa16b90045ad/css/ie7.css" rel="stylesheet" type="text/css" media="all" /><![endif]-->
    <!-- Live Reload
    <script>
    window.brunch = window.brunch || {};
    window.brunch['auto-reload'] = {enabled: true};
    </script>
    -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/media/themes/50057774fc96aa16b90045ad/js/jquery-1.7.2.min.js"><\\/script>')</script>
    <script src="/media/themes/50057774fc96aa16b90045ad/js/lib-combined.js"></script>
    <script src="/media/themes/50057774fc96aa16b90045ad/js/global-combined.js"></script>
    <script src="/media/themes/50057774fc96aa16b90045ad/js/jquery.colorbox_1.3_a.js"></script>
    <script>require('initialize');</script>
    <!-- Google Analytics Code: POP customized-->
    <script type="text/javascript">
        var _gaq = _gaq || [];
            (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

            var pop = document.createElement('script'); pop.type = 'text/javascript'; pop.async = true;
            pop.src = '/media/themes/50057774fc96aa16b90045ad/js/popanalytics.js';
            var js = document.getElementsByTagName('script')[0]; js.parentNode.insertBefore(pop, js);
        })();
    </script>
    <!-- END Google Analytics Code: POP customized-->










</head>
<body class="no-js press leadership">
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
<script type="text/javascript">
  \$(document).ready(function(){
    \$("a#sms").colorbox({inline: true, href: "#smsalerts", close: "", onComplete: function(){\$("a#smsAlertsClose").focus();}});
  });
</script>
<div style="display: none;">
  <div id="smsalerts">




       <div class="h3">SMS press alerts</div>
    <p>If you would like to receive SMS updates from Target Pressroom, text keyword TARGET to (612) 225-9509.</p>


    <p><a href="#" id="smsAlertsClose">Close</a></p>
    <script>\$(function(){ \$("a#smsAlertsClose").click(function(){\$("a#sms").colorbox.close(); return false;}) })</script>
  </div>
</div>
    <noscript>
        <h2><center>This website requires Javascript.</center></h2>
    </noscript>
    <nav role="navigation">
      <ul class="skip-links">
        <li><a href="#nav-main" class="navJump accessLabel">Skip to main navigation</a></li>
        <li><a href="#main-content" class="accessLabel">Skip to content</a></li>
        <li><a href="#global-footer" class="accessLabel">Skip to footer</a></li>
      </ul>
    </nav>
    <div id="wrapper" class="press leadership">
        <header id="global-header" role="banner">
            <div class="inner header" id="top">
                <div class="logo"><a href="//corporate.target.com"><img src="/media/themes/50057774fc96aa16b90045ad/images/target-bullseye.png" alt="Target Corporation" /></a></div>
                <div class="nav-primary redBtn" id="menuBtn"><a href="#menu" tabIndex="-1"><span class="offscreen">Menu</span></a></div>
                <form role="search" method="get" action="//corporate.target.com/search-results">
                    <label for="search" id="searchLabel" class="redBtn"><span class="offscreen"> Search:</span></label>
                    <div id="searchFields">
                        <div>
                        <input type="text" id="search" name="q" placeholder="can we help you find something?" />
                        <input type="submit" value="go" class="submit redBtn" />
                        </div>
                    </div>
                </form>





\t\t\t\t<nav id="nav-main" role="navigation">
\t\t\t\t    <ul id="nav-primary">
\t\t\t\t    \t<li id="nav-home"><a href="http://corporate.target.com/" class="nav-item-primary" tabIndex="-1">home</a></li>
\t\t\t\t        <li id="nav-about">
\t\t\t\t        \t<a href="http://corporate.target.com/about/" class="nav-item-primary">
\t\t\t\t            \t<span class="defaultText">about Target</span>
\t\t\t\t            \t<span class="updateLabel"></span>
\t\t\t\t            \t<span class="accessLabel"> expand</span>
\t\t\t\t            </a>
\t\t\t\t            <nav class="nav-secondary">
\t\t\t\t                <span class="indicator"></span>
\t\t\t\t            \t<div class="subnav-wrapper">
\t\t\t\t                <ul>
\t\t\t\t                    <li><a href="http://corporate.target.com/about/">about Target home</a></li>
\t\t\t\t                    <li><a href="http://corporate.target.com/about/mission-values/">mission &amp; values</a></li>
\t\t\t\t                    <li><a href="http://corporate.target.com/about/shopping-experience/">the shopping experience</a></li>
\t\t\t\t                    <li><a href="http://corporate.target.com/about/design-innovation/">design &amp; innovation</a></li>
\t\t\t\t                </ul>
\t\t\t\t                <ul>
\t\t\t\t                    <li><a href="http://corporate.target.com/about/history/">history</a></li>
\t\t\t\t                    <li><a href="http://corporate.target.com/about/awards-recognition/">awards &amp; recognition</a></li>
\t\t\t\t                \t<li><a href="http://corporate.target.com/about/suppliers/">suppliers</a></li>
\t\t\t\t                </ul>
\t\t\t\t                <div class="clear"></div>
\t\t\t\t                </div>
\t\t\t\t                <span class="img"></span>
\t\t\t\t            </nav>
\t\t\t\t        </li>
\t\t\t\t        <li id="nav-careers">
\t\t\t\t        \t<a href="http://corporate.target.com/careers/" class="nav-item-primary">
\t\t\t\t        \t\t<span class="defaultText">careers</span>
\t\t\t\t            \t<span class="updateLabel"></span>
\t\t\t\t            \t<span class="accessLabel"> expand</span>
\t\t\t\t        \t</a>
\t\t\t\t            <nav class="nav-secondary">
\t\t\t\t                <span class="indicator"></span>
\t\t\t\t            \t<div class="subnav-wrapper">
\t\t\t\t                <ul>
\t\t\t\t                    <li><a href="http://corporate.target.com/careers/">careers home</a></li>
\t\t\t\t                    <li><a href="http://corporate.target.com/careers/career-areas/">career areas</a></li>
\t\t\t\t                    <li><a href="http://corporate.target.com/careers/culture/">culture</a></li>
\t\t\t\t                    <li><a href="http://corporate.target.com/careers/benefits/">benefits</a></li>
\t\t\t\t                    <li><a href="http://corporate.target.com/careers/global-locations/">global locations</a></li>
\t\t\t\t                    <li><a href="http://corporate.target.com/careers/recruiting-events/">recruiting events</a></li>
\t\t\t\t                    <li><a href="http://corporate.target.com/careers/faqs/">careers FAQs</a></li>
\t\t\t\t                </ul>
\t\t\t\t                <ul>
\t\t\t\t                \t<li><strong>explore by background:</strong></li>
\t\t\t\t                \t<li><a href="http://corporate.target.com/careers/college-students/">college students</a></li>
\t\t\t\t                \t<li><a href="http://corporate.target.com/careers/mbas-graduate-degrees/">MBAs &amp; graduate degrees</a></li>
\t\t\t\t                \t<li><a href="http://corporate.target.com/careers/healthcare/">healthcare</a></li>
\t\t\t\t                </ul>
\t\t\t\t                <div class="clear"></div>
\t\t\t\t                </div>
\t\t\t\t                <span class="img"></span>
\t\t\t\t            </nav>
\t\t\t\t        </li>
\t\t\t\t        <li id="nav-corporate-responsibility">
\t\t\t\t        \t<a href="http://corporate.target.com/corporate-responsibility/" class="nav-item-primary">
\t\t\t\t        \t\t<span class="defaultText">corporate responsibility</span>
\t\t\t\t            \t<span class="updateLabel"></span>
\t\t\t\t            \t<span class="accessLabel"> expand</span>
\t\t\t\t        \t</a>
\t\t\t\t            <nav class="nav-secondary">
\t\t\t\t                <span class="indicator"></span>
\t\t\t\t            \t<div class="subnav-wrapper">
\t\t\t\t                <ul>
\t\t\t\t                    <li><a href="http://corporate.target.com/corporate-responsibility/">corporate responsibility home</a></li>
\t\t\t\t                    <li><a href="http://corporate.target.com/corporate-responsibility/ceo-message/">CEO message</a></li>
\t\t\t\t                    <li><a href="http://corporate.target.com/corporate-responsibility/grants/">grants</a></li>
\t\t\t\t                    <li><a href="http://corporate.target.com/corporate-responsibility/community-events/">community events</a></li>
\t\t\t\t                    <li><a href="http://corporate.target.com/corporate-responsibility/goals-reporting/">goals &amp; reporting</a></li>
\t\t\t\t                    <li><a href="http://corporate.target.com/corporate-responsibility/stakeholder-engagement/">stakeholder engagement</a></li>
\t\t\t\t                    <li><a href="http://corporate.target.com/corporate-responsibility/civic-activity/">civic activity</a></li>
\t\t\t\t                </ul>
\t\t\t\t                <ul>
\t\t\t\t                    <li class="nested"><strong>areas of commitment:</strong>
\t\t\t\t                    <ul>
\t\t\t\t                        <li><a href="http://corporate.target.com/corporate-responsibility/education/">education</a></li>
\t\t\t\t                        <li><a href="http://corporate.target.com/corporate-responsibility/environment/">environment</a></li>
\t\t\t\t                        <li><a href="http://corporate.target.com/corporate-responsibility/health-well-being/">health &amp; well-being</a></li>
\t\t\t\t                        <li><a href="http://corporate.target.com/corporate-responsibility/team-members/">team members</a></li>
\t\t\t\t                        <li><a href="http://corporate.target.com/corporate-responsibility/responsible-sourcing/">responsible sourcing</a></li>
\t\t\t\t                        <li><a href="http://corporate.target.com/corporate-responsibility/safety-preparedness/">safety &amp; preparedness</a></li>
\t\t\t\t                        <li><a href="http://corporate.target.com/corporate-responsibility/volunteerism/">volunteerism</a></li>
\t\t\t\t                    </ul>
\t\t\t\t                    </li>
\t\t\t\t                </ul>
\t\t\t\t                <div class="clear"></div>
\t\t\t\t                </div>
\t\t\t\t                <span class="img"></span>
\t\t\t\t            </nav>
\t\t\t\t        </li>
\t\t\t\t        <li id="nav-investors">
\t\t\t\t\t\t  <a href="http://investors.target.com/phoenix.zhtml?c=65828&p=irol-IRHome" class="nav-item-primary">
\t\t\t\t\t\t\t    <span class="defaultText">investors</span>
\t\t\t\t            \t<span class="updateLabel"></span>
\t\t\t\t            \t<span class="accessLabel"> expand</span>
\t\t\t\t\t\t  </a>
\t\t\t\t\t\t    <nav class="nav-secondary">
\t\t\t\t\t\t        <span class="indicator"></span>
\t\t\t\t\t\t      <div class="subnav-wrapper">
\t\t\t\t\t\t        <ul>
\t\t\t\t\t\t            <li><a href="http://investors.target.com/phoenix.zhtml?c=65828&p=irol-IRHome">investors home</a></li>
\t\t\t\t\t\t            <li><a href="http://investors.target.com/phoenix.zhtml?c=65828&p=irol-homeProfile">corporate overview</a></li>
\t\t\t\t\t\t            <li><a href="http://investors.target.com/phoenix.zhtml?c=65828&p=irol-stockquotechart">stock information</a></li>
\t\t\t\t\t\t            <li><a href="http://investors.target.com/phoenix.zhtml?c=65828&p=irol-reportsAnnual">annual reports</a></li>
\t\t\t\t\t\t            <li><a href="http://investors.target.com/phoenix.zhtml?c=65828&p=irol-sec">sec filings</a></li>
\t\t\t\t\t\t            <li><a href="http://investors.target.com/phoenix.zhtml?c=65828&p=irol-estimates">analyst coverage</a></li>
\t\t\t\t\t\t        </ul>
\t\t\t\t\t\t        <ul>
\t\t\t\t\t\t            <li><a href="http://investors.target.com/phoenix.zhtml?c=65828&p=irol-news&nyo=0">financial news</a></li>
\t\t\t\t\t\t            <li><a href="http://investors.target.com/phoenix.zhtml?c=65828&p=irol-summaryfinancial">summary financials</a></li>
\t\t\t\t\t\t            <li><a href="http://investors.target.com/phoenix.zhtml?c=65828&p=irol-calendar">events &amp; presentations</a></li>
\t\t\t\t\t\t            <li><a href="http://investors.target.com/phoenix.zhtml?c=65828&p=irol-govHighlights">corporate governance</a></li>
\t\t\t\t\t\t            <li><a href="http://investors.target.com/phoenix.zhtml?c=65828&p=irol-stockpurchase">shareholder services</a></li>
\t\t\t\t\t\t        </ul>
\t\t\t\t\t\t        <div class="clear"></div>
\t\t\t\t\t\t        </div>
\t\t\t\t\t\t        <span class="img"></span>
\t\t\t\t\t\t    </nav>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li id="nav-press" class="last">
\t\t\t\t\t\t  <a href="/press/" class="nav-item-primary current">
\t\t\t\t\t\t\t    <span class="defaultText">press</span>
\t\t\t\t            \t<span class="updateLabel"></span>
\t\t\t\t            \t<span class="accessLabel"> expand</span>
\t\t\t\t\t\t  </a>
\t\t\t\t\t\t    <nav class="nav-secondary">
\t\t\t\t\t\t        <span class="indicator"></span>
\t\t\t\t\t\t      <div class="subnav-wrapper">
\t\t\t\t\t\t        <ul>
\t\t\t\t\t\t            <li><a href="/">press home</a></li>
\t\t\t\t\t\t            <li><a href="/corporate">corporate fact sheet</a></li>
\t\t\t\t\t\t            <li><a href="/leadership">leadership</a></li>
\t\t\t\t\t\t            <li><a href="/news">releases</a></li>
\t\t\t\t\t\t        </ul>
\t\t\t\t\t\t        <ul>
\t\t\t\t\t\t            <li><a href="/multimedia">multimedia library</a></li>
\t\t\t\t\t\t            <li><a href="/faq">media FAQs</a></li>
\t\t\t\t\t\t            <li><a href="/presskits">press kits</a></li>
\t\t\t\t\t\t        </ul>
\t\t\t\t\t\t        <div class="clear"></div>
\t\t\t\t\t\t        </div>
\t\t\t\t\t\t        <span class="img"></span>
\t\t\t\t\t\t    </nav>
\t\t\t\t\t\t</li>
\t\t\t\t    </ul>
\t\t\t\t    <ul id="nav-utility">
\t\t\t\t\t\t<li><a href="http://www.target.com/">shop Target.com</a></li>
\t\t                <li><a href="http://www.target.com/store-locator/find-stores">find a store</a></li>
\t\t\t\t\t\t<li><a href="http://www.target.com/HelpContent?help=/sites/html/TargetOnline/help/contact_us/contact_us.html">contact us</a></li>
\t\t\t\t\t</ul>
\t\t\t\t</nav>


            </div>
        </header>

        <div id="main-content">
        <!-- START PAGE CONTENT -->
            <div class="inner two-column">

                <nav class="breadcrumb">

                    <ul>
                        <li><a href="//corporate.target.com/">home</a></li>
                        <li><a href="/">press</a></li>
                        <li><a href="/leadership" class="current">leadership</a></li>
                    </ul>

                </nav><!-- end of .breadcrumb -->

                <section class="two-column-primary-wrapper" >
\t\t\t\t
<h1 id="leadership">leadership</h1>
<section class="primaryColumn">
    <article class="meetExecutives">
      <h2></h2>
      <ul class="no-bullets" id="executives">

          <li class="odd">

            <img src="https://s3.amazonaws.com/cms.ipressroom.com/18/files/20125/5007972d29371a1ed20012c3_228006_greggsteinhafel/228006_greggsteinhafel_s.jpg" alt="Gregg  W.  Steinhafel" title="Gregg  W.  Steinhafel" style="width:96px;height:auto;">

            <h3 class="executiveName"><a href="/leadership/gregg-w-steinhafel">Gregg  W.  Steinhafel</a></h3>
            <span class="executiveTitle">Chairman, President and Chief Executive Officer</span>
            <div class="clear"></div>
            <p>Gregg Steinhafel is chairman of the board, president and chief executive officer of Target Corporation (NYSE: TGT). <a href="/leadership/gregg-w-steinhafel">read more <span class="offscreen">about Gregg  W.  Steinhafel</span></a></p>
          </li>

          <li class="">

            <img src="https://s3.amazonaws.com/cms.ipressroom.com/18/files/20125/500799b629371a1ed200131b_228005_Tim_Baer_0549/228005_Tim_Baer_0549_s.jpg" alt="Timothy R. Baer" title="Timothy R. Baer" style="width:96px;height:auto;">

            <h3 class="executiveName"><a href="/leadership/timothy-r-baer-228005">Timothy R. Baer</a></h3>
            <span class="executiveTitle">Executive Vice President, General Counsel and Corporate Secretary</span>
            <div class="clear"></div>
            <p>Baer&#39;s responsibilities include the law department, government affairs, assets protection, corporate risk and responsibility, and Target Brands, Inc. <a href="/leadership/timothy-r-baer-228005">read more <span class="offscreen">about Timothy R. Baer</span></a></p>
          </li>

          <li class="odd">

            <img src="https://s3.amazonaws.com/cms.ipressroom.com/18/files/20125/500799e629371a1ed2001321_229462_Executive_Portrait_high_res/229462_Executive_Portrait_high_res_s.jpg" alt="Anthony S. Fisher" title="Anthony S. Fisher" style="width:96px;height:auto;">

            <h3 class="executiveName"><a href="/leadership/tony-fisher">Anthony S. Fisher</a></h3>
            <span class="executiveTitle">President, Target Canada</span>
            <div class="clear"></div>
            <p>Fisher is responsible for building the Target Canada team, establishing headquarters and leading the day-to-day operations of the corporation’s expansion into Canada.    <a href="/leadership/tony-fisher">read more <span class="offscreen">about Anthony S. Fisher</span></a></p>
          </li>

          <li class="">

            <img src="https://s3.amazonaws.com/cms.ipressroom.com/18/files/20125/5007972329371a1ed20012c1_228004_johngriffith/228004_johngriffith_s.jpg" alt="John  D.  Griffith" title="John  D.  Griffith" style="width:96px;height:auto;">

            <h3 class="executiveName"><a href="/leadership/john-d-griffith-228004">John  D.  Griffith</a></h3>
            <span class="executiveTitle">Executive Vice President, Property Development</span>
            <div class="clear"></div>
            <p>Griffith leads the real estate, construction, architecture, engineering, store planning, design, facilities management, property operations and Target Commercial Interiors teams <a href="/leadership/john-d-griffith-228004">read more <span class="offscreen">about John  D.  Griffith</span></a></p>
          </li>

          <li class="odd">

            <img src="https://s3.amazonaws.com/cms.ipressroom.com/18/files/20125/500817b629371a1ed2001522_232460_Executive_Portrait_Jeff_Jones-2/232460_Executive_Portrait_Jeff_Jones-2_s.jpg" alt="Jeffrey J. Jones II" title="Jeffrey J. Jones II" style="width:96px;height:auto;">

            <h3 class="executiveName"><a href="/leadership/jeffrey-j-jones-ii-232460">Jeffrey J. Jones II</a></h3>
            <span class="executiveTitle">Executive Vice President and Chief Marketing Officer</span>
            <div class="clear"></div>
            <p>Jeff Jones joined Target in 2012 as executive vice president and chief marketing officer. <a href="/leadership/jeffrey-j-jones-ii-232460">read more <span class="offscreen">about Jeffrey J. Jones II</span></a></p>
          </li>

          <li class="">

            <img src="https://s3.amazonaws.com/cms.ipressroom.com/18/files/20125/5007970a29371a1ed20012bd_228001_jodeenkozlak/228001_jodeenkozlak_s.jpg" alt="Jodeen A. Kozlak" title="Jodeen A. Kozlak" style="width:96px;height:auto;">

            <h3 class="executiveName"><a href="/leadership/jodeen-a-kozlak-228001">Jodeen A. Kozlak</a></h3>
            <span class="executiveTitle">Executive Vice President, Human Resources</span>
            <div class="clear"></div>
            <p>Kozlak sets the strategy for enterprise talent management and organizational design and alignment as well as team culture and employment brand. <a href="/leadership/jodeen-a-kozlak-228001">read more <span class="offscreen">about Jodeen A. Kozlak</span></a></p>
          </li>

          <li class="odd">

            <img src="https://s3.amazonaws.com/cms.ipressroom.com/18/files/20125/50080dcc29371a1ed20014f3_233032_John_Mulligan/233032_John_Mulligan_s.jpg" alt="John J. Mulligan" title="John J. Mulligan" style="width:96px;height:auto;">

            <h3 class="executiveName"><a href="/leadership/john-mulligan-233032">John J. Mulligan</a></h3>
            <span class="executiveTitle">Executive Vice President and Chief Financial Officer</span>
            <div class="clear"></div>
            <p>Mulligan&#39;s responsibilities include treasury, internal and external financial reporting, financial planning and analysis, financial operations, tax, assurance, investor relations and flight services. <a href="/leadership/john-mulligan-233032">read more <span class="offscreen">about John J. Mulligan</span></a></p>
          </li>

          <li class="">

            <img src="https://s3.amazonaws.com/cms.ipressroom.com/18/files/20125/500796c029371a1ed20012b7_227994_tinaschiel/227994_tinaschiel_s.jpg" alt="Tina M. Schiel" title="Tina M. Schiel" style="width:96px;height:auto;">

            <h3 class="executiveName"><a href="/leadership/tina-m-schiel-227994">Tina M. Schiel</a></h3>
            <span class="executiveTitle">Executive Vice President, Stores</span>
            <div class="clear"></div>
            <p>Schiel&#39;s responsibilities included new formats such as City Target and international opportunities. <a href="/leadership/tina-m-schiel-227994">read more <span class="offscreen">about Tina M. Schiel</span></a></p>
          </li>

          <li class="odd">

            <img src="https://s3.amazonaws.com/cms.ipressroom.com/18/files/20125/500796bc29371a1ed20012b6_227992_kathryntesija/227992_kathryntesija_s.jpg" alt="Kathryn A. Tesija" title="Kathryn A. Tesija" style="width:96px;height:auto;">

            <h3 class="executiveName"><a href="/leadership/kathryn-a-tesija-227992">Kathryn A. Tesija</a></h3>
            <span class="executiveTitle">Executive Vice President, Merchandising and Supply Chain</span>
            <div class="clear"></div>
            <p>Tesija oversees all merchandising functions of product design and development, sourcing, inventory management, and the end-to-end global supply chain. <a href="/leadership/kathryn-a-tesija-227992">read more <span class="offscreen">about Kathryn A. Tesija</span></a></p>
          </li>

          <li class="">

            <img src="https://s3.amazonaws.com/cms.ipressroom.com/18/files/20125/500796aa29371a1ed20012b4_227990_layshaward/227990_layshaward_s.jpg" alt="Laysha L. Ward" title="Laysha L. Ward" style="width:96px;height:auto;">

            <h3 class="executiveName"><a href="/leadership/laysha-l-ward-227990">Laysha L. Ward</a></h3>
            <span class="executiveTitle">President, Community Relations</span>
            <div class="clear"></div>
            <p>Laysha Ward is president of Community Relations for Target and a member of its executive committee. <a href="/leadership/laysha-l-ward-227990">read more <span class="offscreen">about Laysha L. Ward</span></a></p>
          </li>

      </ul>

    <h3>other executives</h3>

<ul class="otherExecutives no-bullets linklist">
\t<li>
\t<h3 class="executiveName">Patricia Adams</h3>
\t<span class="executiveTitle">Senior Vice President, Merchandising, Apparel and Accessories</span></li>
\t<li>
\t<h3 class="executiveName">Aaron Alt</h3>
\t<span class="executiveTitle">Senior Vice President, Business Development and Treasurer</span></li>
\t<li>
\t<h3 class="executiveName">Stacia Andersen</h3>
\t<span class="executiveTitle">Senior Vice President, Merchandising, Home</span></li>
\t<li>
\t<h3 class="executiveName">Jose Barra</h3>
\t<span class="executiveTitle">Senior Vice President, Merchandising, Health and Beauty</span></li>
\t<li>
\t<h3 class="executiveName">Bryan Berg</h3>
\t<span class="executiveTitle">Senior Vice President, Stores, Target Canada</span></li>
\t<li>
\t<h3 class="executiveName">Stephen Brinkley</h3>
\t<span class="executiveTitle">Senior Vice President, Stores</span></li>
\t<li>
\t<h3 class="executiveName">John Butcher</h3>
\t<span class="executiveTitle">Senior Vice President, Merchandising, Target Canada</span></li>
\t<li>
\t<h3 class="executiveName">Tom Butterfield</h3>
\t<span class="executiveTitle">Senior Vice President, Strategy and Enterprise Architecture</span></li>
\t<li>
\t<h3 class="executiveName">Casey Carl</h3>
\t<span class="executiveTitle">President, Multichannel, and Senior Vice President, Enterprise Strategy</span></li>
\t<li>
\t<h3 class="executiveName">Tim Curoe</h3>
\t<span class="executiveTitle">Senior Vice President, Talent &amp; Organizational Effectiveness</span></li>
\t<li>
\t<h3 class="executiveName">Barbara Dugan</h3>
\t<span class="executiveTitle">Senior Vice President, Target Sourcing Services</span></li>
\t<li>
\t<h3 class="executiveName">Bryan Everett</h3>
\t<span class="executiveTitle">Senior Vice President, Store Operations</span></li>
\t<li>
\t<h3 class="executiveName">Juan Galarraga</h3>
\t<span class="executiveTitle">Senior Vice President, Stores</span></li>
\t<li>
\t<h3 class="executiveName">Jason Goldberger</h3>
\t<span class="executiveTitle">Senior Vice President, Target.com and Mobile</span></li>
\t<li>
\t<h3 class="executiveName">Rick Gomez</h3>
\t<span class="executiveTitle">Senior Vice President, Brand &amp; Category Marketing</span></li>
\t<li>
\t<h3 class="executiveName">Corey Haaland</h3>
\t<span class="executiveTitle">Senior Vice President, Financial Planning Analysis and Tax</span></li>
\t<li>
\t<h3 class="executiveName">Cynthia Ho</h3>
\t<span class="executiveTitle">Senior Vice President, Target Sourcing Services</span></li>
\t<li>
\t<h3 class="executiveName">Derek Jenkins</h3>
\t<span class="executiveTitle">Senior Vice President, External Affairs, Target Canada</span></li>
</ul>

<ul class="otherExecutives no-bullets linklist last">
\t<li>
\t<h3 class="executiveName">Keri Jones</h3>
\t<span class="executiveTitle">Senior Vice President, Merchandise Planning</span></li>
\t<li>
\t<h3 class="executiveName">Navneet Kapoor</h3>
\t<span class="executiveTitle">President and Managing Director, Target India</span></li>
\t<li>
\t<h3 class="executiveName">Scott Kennedy</h3>
\t<span class="executiveTitle">President, Target Financial and Retail Services</span></li>
\t<li>
\t<h3 class="executiveName">Timothy A. Mantel</h3>
\t<span class="executiveTitle">President, Target Sourcing Services</span></li>
\t<li>
\t<h3 class="executiveName">Todd Marshall</h3>
\t<span class="executiveTitle">Senior Vice President, Marketing</span></li>
\t<li>
\t<h3 class="executiveName">John Morioka</h3>
\t<span class="executiveTitle">Senior Vice President, Merchandising, Target Canada</span></li>
\t<li>
\t<h3 class="executiveName">Scott Nelson</h3>
\t<span class="executiveTitle">Senior Vice President, Real Estate</span></li>
\t<li>
\t<h3 class="executiveName">Scott Nygaard</h3>
\t<span class="executiveTitle">Senior Vice President, Merchandising, Hardlines</span></li>
\t<li>
\t<h3 class="executiveName">Janna Potts</h3>
\t<span class="executiveTitle">Senior Vice President, Stores and Distribution, Target Canada</span></li>
\t<li>
\t<h3 class="executiveName">Mike Robbins</h3>
\t<span class="executiveTitle">Senior Vice President, Distribution</span></li>
\t<li>
\t<h3 class="executiveName">Mark Schindele</h3>
\t<span class="executiveTitle">Senior Vice President, Merchandising Operations</span></li>
\t<li>
\t<h3 class="executiveName">Samir Shah</h3>
\t<span class="executiveTitle">Senior Vice President, Stores</span></li>
\t<li>
\t<h3 class="executiveName">Cary Strouse</h3>
\t<span class="executiveTitle">Senior Vice President, Stores</span></li>
\t<li>
\t<h3 class="executiveName">Todd Waterbury</h3>
\t<span class="executiveTitle">Senior Vice President, Creative</span></li>
\t<li>
\t<h3 class="executiveName">Judy Werthauser</h3>
\t<span class="executiveTitle">Senior Vice President, Human Resources, Headquarters</span></li>
\t<li>
\t<h3 class="executiveName">Jane Windmeier</h3>
\t<span class="executiveTitle">Senior Vice President, Global Finance Systems</span></li>
</ul>


    </article>
</section>

                <div class="clear"></div>
\t\t\t\t</section>

                <aside class="sidebar">
\t\t\t\t
                \t



   <div class="searchPress sidebar-section" id="press">
<h3>search press</h3>
 <form role="search" method="get" action="/search">
\t<label id="searchPressLabel" for="press-search"><span class="offscreen">Search:</span></label>
\t<input type="search" placeholder="search by keyword" name="q" id="press-search">
\t<input type="submit" class="submit redBtn" value="go">
\t<div class="clear"></div>
</form>
<h4>popular tags</h4>
<span class="popular-tags">
\t<a href="/news?c=19039">apparel &amp; accessories</a> <b>&middot;</b>
\t<a href="/news?c=18082">corporate social responsibility</a> <b>&middot;</b>
\t<a href="/news?c=18093">disaster relief</a> <b>&middot;</b>
\t<a href="/news?c=18072">financial/strategic announcements</a> <b>&middot;</b>
\t<a href="/news?c=18115">holiday</a> <b>&middot;</b>
\t<a href="/news?c=18102">sales &amp; earnings</a> <b>&middot;</b>
\t<a href="/news?c=18080">real estate</a> <b>&middot;</b>
\t<a href="/news?c=18078">seasonal</a> <b>&middot;</b>
\t<a href="/news?c=18125">stores</a>
</span>
</div>






   <div class="mediaHotline sidebar-section">
<h3>media hotline</h3>
<p><strong>p:</strong> <span>(612) 696-3400</span><br />
<strong>e:</strong> <a href="mailto:press@target.com">press@target.com</a></p>

<p>We strive to return all media calls within one business day.</p>

<h3>guest relations</h3>

<p>Guests should contact 1.800.440.0680 or visit <a href="http://www.target.com/HelpContent?help=/sites/html/TargetOnline/help/contact_us/contact_us.html" target="_blank" title="">Target Help.</a></p>

<h3>investor relations</h3>

<p>John Hulbert, (612) 761-6627</p>

<h3>advertising inquiries</h3>
<strong>e:</strong> <a href="mailto:media@target.com" target="_blank">media@target.com</a></div>




<div class="hr-wrapper bg"><div class="hr"><hr></div></div>
<div class="targetFeeds sidebar-section">

  <h3>sign up for alerts &amp; feeds</h3>
  <ul class="social">

    <li><a href="/rss" id="rss"><span></span> Target rss feeds</a></li>

    <li><a href="/alerts" id="email"><span></span> email alerts</a></li>

    <li><a href="/" id="sms"><span></span> sms alerts</a></li>

  </ul>

</div>


<div class="hr-wrapper bg"><div class="hr"><hr></div></div>
<div class="targetSocial sidebar-section">

  <h3>stay connected</h3>
  <ul class="social">

    <li><a href="https://twitter.com/abullseyeview" title="Link opens in a new window" target="_blank" class="skip-js" id="twitter"><span></span> A Bullseye View on Twitter<span class="offscreen"> opens in a new window</span></a></li>

    li><a href="http://www.facebook.com/target" title="Link opens in a new window" target="_blank" class="skip-js" id="facebook"><span></span> Target on Facebook<span class="offscreen"> opens in a new window</span></a></li>

    <li><a href="http://targetstyle.tumblr.com/" title="Link opens in a new window" target="_blank" class="skip-js" id="tumblr"><span></span> Target on Tumblr<span class="offscreen"> opens in a new window</span></a></li>

    <li><a href="http://pinterest.com/target/" title="Link opens in a new window" target="_blank" class="skip-js" id="pinterest"><span></span> Target on Pinterest<span class="offscreen"> opens in a new window</span></a></li>

    <li><a href="http://www.youtube.com/target" title="Link opens in a new window" target="_blank" class="skip-js" id="youtube"><span></span> Target on Youtube<span class="offscreen"> opens in a new window</span></a></li>

  </ul>
    <a href="/social">view all social media</a>
</div>



<div class="hr-wrapper bg"><div class="hr"><hr></div></div>
<div class="learnMoreAboutTarget sidebar-section">

  <h3>learn more about Target</h3>
  <ul class="social">

    <li><a href="http://pressroom.target.ca" title="Link opens in a new window" target="_blank" class="skip-js" > Target Canada Pressroom <span class="offscreen"> opens in a new window</span></a></li>

    <li><a href="http://investors.target.com/phoenix.zhtml?c=65828&amp;p=irol-homeProfile" title="investor corporate overview" class="skip-js" > investor corporate overview </a></li>

    <li><a href="http://corporate.target.com/about/history/" title="history" class="skip-js" > history </a></li>

    <li><a href="http://corporate.target.com/about/shopping-experience/" title="shopping experience" class="skip-js" > shopping experience </a></li>

    <li><a href="http://corporate.target.com/about/mission-values/" title="mission &amp; values" class="skip-js" > mission &amp; values </a></li>

    <li><a href="http://corporate.target.com/about/awards-recognition" title="awards &amp; recognition" class="skip-js" > awards &amp; recognition </a></li>

  </ul>

</div>


                <div class="hr-wrapper bg"><div class="hr"><hr /></div></div>
                </aside>

                <div class="clear"></div>

            </div>

        </div><!-- end #mainContent -->
\t\t



           <footer id="global-footer" role="banner">
            <div class="footer-inner">
                <nav>
                    <h3 class="offscreen">Additional Site Navigation</h3>
                    <ul id="footer-nav">
                    \t<li><a href="http://corporate.target.com/about/">about Target</a></li>
                        <li><a href="http://corporate.target.com/careers/">careers</a></li>
                        <li><a href="http://corporate.target.com/corporate-responsibility/">corporate responsibility</a></li>
                        <li><a href="http://investors.target.com/">investors</a></li>
                        <li class="last"><a href="http://pressroom.target.com/">press</a></li>
                    </ul>
                    <ul id="footer-utility">
\t\t                <li><a href="http://www.target.com">shop Target.com</a></li>
\t\t                <li><a href="http://www.target.com/store-locator/find-stores">find a store</a></li>
\t\t                <li><a href="http://www.target.com/HelpContent?help=/sites/html/TargetOnline/help/contact_us/contact_us.html">contact us</a></li>
\t\t                <li><a href="http://www.target.com/spot/terms-conditions">terms &amp; conditions</a></li>
\t\t                <li><a href="http://www.target.com/spot/privacy-policy">privacy policy</a></li>
\t\t                <li><a href="http://www.target.com/spot/team-services">team member services</a></li>
\t\t                <li>follow us on <a href="http://facebook.com/target" target="_blank" title="Link opens in a new window">facebook<span class="offscreen">: opens in a new window</span></a> | <a href="http://twitter.com/target" target="_blank" title="Link opens in a new window">twitter<span class="offscreen">: opens in a new window</span></a></li>
\t\t            </ul>
                    <!-- TODO: make year dynamic via server-side -->
                    <p class="copyright">&copy; 2013 Target.com. All rights reserved. The Bullseye Design and Target are registered trademarks of Target Brands, Inc.</p>
                </nav>
            </div>
        </footer>


    </div>
</body>
</html>
"""
}
