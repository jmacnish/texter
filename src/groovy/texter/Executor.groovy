package texter

import groovy.transform.Canonical

/**
 * Fires off another process.
 */
class Executor {

    @Canonical
    static class Result {
        List<String> command
        List<String> environment
        String output
        String error
        Integer exitValue
    }

    static Result execute(List<String> cmd, List<String> environment, int timeoutInMillis) {
        Process process = cmd.execute(environment, null)
        def output = new StringBuilder()
        def error = new StringBuilder()
        process.consumeProcessOutput(output, error)
        process.waitForOrKill(timeoutInMillis)
        new Result(
                command: cmd,
                environment: environment,
                output: output.toString(),
                error: error.toString(),
                exitValue: process.exitValue()
        )

    }

}
