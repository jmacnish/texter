package app

import com.vaadin.data.util.IndexedContainer

/**
 * Container things for organizations.
 */
class OrganizationContainer {

    public static final Object PROPERTY_NAME = "Name"

    static IndexedContainer getContainer() {
        def container = new IndexedContainer()
        container.addContainerProperty(PROPERTY_NAME, String.class, null)
        container
    }

}
