package app

import com.vaadin.data.Container
import com.vaadin.data.Item
import com.vaadin.data.Property
import com.vaadin.data.util.IndexedContainer
import com.vaadin.event.Action
import com.vaadin.event.FieldEvents
import com.vaadin.event.ItemClickEvent
import com.vaadin.grails.Grails
import com.vaadin.server.Page
import com.vaadin.server.Sizeable
import com.vaadin.server.VaadinRequest
import com.vaadin.ui.*
import groovy.json.JsonSlurper
import groovy.transform.Canonical
import texter.Content
import texter.ContentService
import texter.Executor
import texter.Source

import java.nio.file.Files
import java.nio.file.Paths
import java.util.concurrent.TimeUnit

/**
 * Main UI for texter web app.
 */
class TexterUI extends UI {

    private static final String NewFieldValue = '<Double Click to Edit>'
    private static final int Timeout = TimeUnit.SECONDS.toMillis(40)

    @Canonical
    class ItemPropertyId {
        Object itemId
        Object propertyId
    }

    @Override
    protected void init(VaadinRequest vaadinRequest) {

        Set markedRows = [] as Set
        Set selectedRows = [] as Set
        VerticalLayout layout = new VerticalLayout()

        TabSheet tabsheet = new TabSheet()
        layout.addComponent(tabsheet)

        VerticalLayout tab1 = new VerticalLayout();
        tabsheet.addTab(tab1, 'Sources')

        def urlTable = new Table()
        urlTable.setSizeFull()
        urlTable.setSelectable(true)
        urlTable.setMultiSelect(false)
        urlTable.setImmediate(true)
        urlTable.setEditable(true)

        final IndexedContainer urlContainer = UrlContainer.getContainer()

        List<Source> sources = Grails.get(ContentService).findSources()

        int urlId = 0
        sources.each { Source source ->
            Item item = urlContainer.addItem(urlId++)
            item.getItemProperty(UrlContainer.PROPERTY_SOURCE_ID).setValue(source.id)
            item.getItemProperty(UrlContainer.PROPERTY_DESCRIPTION).setValue(source.description)
            item.getItemProperty(UrlContainer.PROPERTY_URL).setValue(source.url)
            Content content = Grails.get(ContentService).findLastContent(source)
            String dateStr = ""
            if (content != null) {
                dateStr = content.dateCreated.toGMTString()
            }
            item.getItemProperty(UrlContainer.PROPERTY_DATE).setValue(dateStr)
        }

        urlTable.setContainerDataSource(urlContainer)
        urlTable.setVisibleColumns([UrlContainer.PROPERTY_DESCRIPTION, UrlContainer.PROPERTY_URL, UrlContainer.PROPERTY_DATE] as Object[])
        urlTable.setColumnReorderingAllowed(false)
        urlTable.setColumnCollapsingAllowed(false)
        urlTable.setColumnHeaders(['Description', 'URL', 'Last Fetch Date'] as String[])
        urlTable.setRowHeaderMode(Table.RowHeaderMode.ICON_ONLY)
        urlTable.setColumnExpandRatio(UrlContainer.PROPERTY_URL, 1)
        urlTable.setColumnExpandRatio(UrlContainer.PROPERTY_DESCRIPTION, 1)
        urlTable.setColumnExpandRatio(UrlContainer.PROPERTY_DATE, 1)

        final Action actionMark = new Action("Mark")
        final Action actionUnmark = new Action("Unmark")

        urlTable.addActionHandler(new Action.Handler() {
            @Override
            public Action[] getActions(final Object target, final Object sender) {
                if (markedRows.contains(target)) {
                    return [actionUnmark] as Action[]
                } else {
                    return [actionMark] as Action[]
                }
            }

            @Override
            public void handleAction(final Action action, final Object sender,
                                     final Object target) {
                if (actionMark == action) {
                    markedRows.add(target)
                } else if (actionUnmark == action) {
                    markedRows.remove(target)
                }
                urlTable.markAsDirtyRecursive()
            }

        })

        urlTable.setCellStyleGenerator(new Table.CellStyleGenerator() {
            @Override
            public String getStyle(final Table source, final Object itemId,
                                   final Object propertyId) {
                String style = null;
                if (propertyId == null && markedRows.contains(itemId)) {
                    style = "marked";
                }
                return style;
            }

        })

        urlTable.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(final Property.ValueChangeEvent event) {
                selectedRows.clear()
                selectedRows += event.getProperty().getValue()
                Item item = urlTable.getItem(event.getProperty().getValue())
                if (item != null) {
                    // Update our source!
                    final Long sourceId = (Long) item.getItemProperty(UrlContainer.PROPERTY_SOURCE_ID).getValue()
                    final String url = (String) item.getItemProperty(UrlContainer.PROPERTY_URL).getValue()
                    final String description = (String) item.getItemProperty(UrlContainer.PROPERTY_DESCRIPTION).getValue()
                    Grails.get(ContentService).updateSource(sourceId, url, description)
                }
            }
        })

        final HashMap<Object, HashMap<Object, Field<?>>> fields = new HashMap<Object, HashMap<Object, Field<?>>>()
        final HashMap<Field<?>, Object> itemIds = new HashMap<Field<?>, Object>()

        // This big mess is to let us dynamically select rows to edit.
        urlTable.setTableFieldFactory(new TableFieldFactory() {
            public Field<?> createField(Container container,
                                        final Object itemId, final Object propertyId, Component uiContext) {
                final TextField tf = new TextField()
                tf.setWidth(100, Sizeable.Unit.EM) // make sure fields are wide when editted!
                tf.setData(new ItemPropertyId(itemId, propertyId))

                // Needed for the generated column
                tf.setImmediate(true)

                // Manage the field in the field storage
                HashMap<Object, Field<?>> itemMap = fields.get(itemId);
                if (itemMap == null) {
                    itemMap = new HashMap<Object, Field<?>>()
                    fields.put(itemId, itemMap)
                }
                itemMap.put(propertyId, tf)

                itemIds.put(tf, itemId)

                tf.setReadOnly(true)
                tf.addFocusListener(new FieldEvents.FocusListener() {
                    public void focus(FieldEvents.FocusEvent event) {
                        //
                    }
                })

                tf.addBlurListener(new FieldEvents.BlurListener() {
                    public void blur(FieldEvents.BlurEvent event) {
                        // Make the entire item read-only
                        HashMap<Object, Field<?>> blurItemMap = fields.get(itemId)
                        for (Field<?> f : blurItemMap.values()) {
                            f.setReadOnly(true)
                        }
                    }
                })

                tf
            }
        })

        // Double click on focussed row to turn on edits.
        urlTable.addItemClickListener(new ItemClickEvent.ItemClickListener() {
            @Override
            public void itemClick(ItemClickEvent event) {
                if (event.isDoubleClick()) {
                    HashMap<Object, Field<?>> focusItemMap = fields.get(event.getItemId())
                    for (Field<?> f : focusItemMap.values()) {
                        f.setReadOnly(false)
                    }
                }
            }
        })

        // Select our first row.
        urlTable.setSelectable(true)
        urlTable.select(urlTable.getItemIds().toArray()[0])
        urlTable.focus()

        tab1.addComponent(urlTable)

        // Create a horizontal arrangement of buttons.  Put this at the bottom of the first tab.
        HorizontalLayout buttonLayout = new HorizontalLayout()
        Panel buttonPanel = new Panel('Actions', buttonLayout)

        // Load Button: Load selected URL
        Button loadButton = new Button("Fetch")
        loadButton.addClickListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
                if (selectedRows.size() > 0) {
                    Object[] row = selectedRows.first()
                    Item item = urlContainer.getItem(row[0])
                    if (item.getItemProperty(UrlContainer.PROPERTY_URL).getValue().equals(NewFieldValue) == false) { // A sad, sad hack
                        final Long sourceId = (Long) item.getItemProperty(UrlContainer.PROPERTY_SOURCE_ID).getValue()
                        final Content content = Grails.get(ContentService).fetchContentFromSource(sourceId)
                        if (content != null) {
                            item.getItemProperty(UrlContainer.PROPERTY_DATE).setValue(content.dateCreated.toGMTString())
                            Notification.show("Content Fetched from URL " + content.source.url)
                        } else {
                            Notification.show("Error fetching content from URL " + item.getItemProperty(UrlContainer.PROPERTY_URL).getValue())
                        }
                    }
                }
            }
        })
        buttonLayout.addComponent(loadButton)

        // Extract Button: Extract content from selected URL
        // Click handler for this is LATER (after we add tab2)
        Button extractButton = new Button('Extract')
        buttonLayout.addComponent(extractButton)

        // Allow changes to the table.
        Button createButton = new Button("New")

        createButton.addClickListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
                Item item = urlContainer.addItem(urlId++)
                final Source source = Grails.get(ContentService).createNewSource()
                item.getItemProperty(UrlContainer.PROPERTY_SOURCE_ID).setValue(source.id)
                item.getItemProperty(UrlContainer.PROPERTY_DESCRIPTION).setValue(source.description)
                item.getItemProperty(UrlContainer.PROPERTY_URL).setValue(source.url)
                item.getItemProperty(UrlContainer.PROPERTY_DATE).setValue("")
            }
        })

        buttonLayout.addComponent(createButton)

        // Add buttons to bottom of tab1.
        tab1.addComponent(buttonPanel)

        // Create a table for displaying people.
        def peopleTable = new Table()
        peopleTable.setSizeFull()
        peopleTable.setImmediate(true)
        final IndexedContainer peopleContainer = PeopleContainer.getContainer()
        peopleTable.setContainerDataSource(peopleContainer)
        peopleTable.setVisibleColumns([PeopleContainer.PROPERTY_NAME, PeopleContainer.PROPERTY_GENDER] as Object[])
        peopleTable.setColumnHeaders(['Name', 'Gender'] as String[])
        peopleTable.setColumnExpandRatio(PeopleContainer.PROPERTY_NAME, 1)
        peopleTable.setColumnWidth(PeopleContainer.PROPERTY_GENDER, 70)
        peopleTable.setRowHeaderMode(Table.RowHeaderMode.ICON_ONLY)

        def titleTable = new Table()
        titleTable.setSizeFull()
        titleTable.setImmediate(true)
        final IndexedContainer titleContainer = TitleContainer.getContainer()
        titleTable.setContainerDataSource(titleContainer)
        titleTable.setVisibleColumns([TitleContainer.PROPERTY_TITLE] as Object[])
        titleTable.setColumnHeaders(['Title'] as String[])
        titleTable.setColumnExpandRatio(TitleContainer.PROPERTY_TITLE, 1)
        titleTable.setRowHeaderMode(Table.RowHeaderMode.ICON_ONLY)

        def organizationTable = new Table()
        organizationTable.setSizeFull()
        organizationTable.setImmediate(true)
        final IndexedContainer organizationContainer = OrganizationContainer.getContainer()
        organizationTable.setContainerDataSource(organizationContainer)
        organizationTable.setVisibleColumns([OrganizationContainer.PROPERTY_NAME] as Object[])
        organizationTable.setColumnHeaders(['Name'] as String[])
        organizationTable.setColumnExpandRatio(OrganizationContainer.PROPERTY_NAME, 1)
        organizationTable.setRowHeaderMode(Table.RowHeaderMode.ICON_ONLY)

        VerticalLayout tab2 = new VerticalLayout()
        tab2.setCaption("Extraction Results")

        Accordion accordion = new Accordion()
        accordion.setSizeFull()
        accordion.addTab(peopleTable, "People", null)
        accordion.addTab(titleTable, "Titles", null)
        accordion.addTab(organizationTable, "Organizations", null)
        tab2.addComponent(accordion)
        tabsheet.addTab(tab2)

        int peopleId = 0

        // Extraction button: Find the URL selected, extract from it.
        extractButton.addClickListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
                if (selectedRows.size() > 0) {
                    Object[] row = selectedRows.first()
                    Item item = urlContainer.getItem(row[0])
                    final Long sourceId = item.getItemProperty(UrlContainer.PROPERTY_SOURCE_ID).getValue()
                    Content content = Grails.get(ContentService).findLastContent(sourceId)
                    if (content != null) {
                        ExtractionResult extractionResult = parse(content.content)

                        // Nuke all existing.
                        organizationContainer.removeAllItems()
                        peopleContainer.removeAllItems()
                        titleContainer.removeAllItems()

                        // Add our extracted results back in.
                        extractionResult.persons.each { Map<String, String> person ->
                            item = peopleContainer.addItem(peopleId++)
                            item.getItemProperty(PeopleContainer.PROPERTY_NAME).setValue(person.get('name'))
                            item.getItemProperty(PeopleContainer.PROPERTY_GENDER).setValue(person.get('gender'))
                        }
                        extractionResult.titles.each { Map<String, String> title ->
                            item = titleContainer.addItem(peopleId++)
                            item.getItemProperty(TitleContainer.PROPERTY_TITLE).setValue(title.get('title'))
                        }
                        extractionResult.organizations.each { Map<String, String> title ->
                            item = organizationContainer.addItem(peopleId++)
                            item.getItemProperty(OrganizationContainer.PROPERTY_NAME).setValue(title.get('name'))
                        }
                        Notification.show('Data extracted')
                    } else {
                        Notification.show('No data to extract: fetch it first')
                    }
                }
            }
        })

        Page.getCurrent().setTitle('Texter: Extract Information from URLs')
        Panel panel = new Panel("Texter: Extract Information from URLs", layout)
        setContent(new Panel("Texter: Extract Information from URLs", layout))
    }

    @Canonical
    static class ExtractionResult {
        List<Map<String, String>> persons
        List<Map<String, String>> organizations
        List<Map<String, String>> dates
        List<Map<String, String>> titles
    }

    public ExtractionResult parse(String urlContent) {
        List<String> environment = [
                'JAVA_OPTS=-Dgate.site.config=/usr/local/gate/gate-7.1-build4485-BIN/gate.xml -Dgate.plugins.home=/usr/local/gate/gate-7.1-build4485-BIN/plugins'
        ]
        File tempOutputDir = Files.createTempDirectory(Paths.get("/tmp"), "texter").toFile()
        File inputFile = new File(tempOutputDir, "input.html")
        inputFile.write(urlContent)
        Executor.Result result = Executor.execute(['/usr/local/textgate/bin/textgate',
                                                   "--inputFile=\"${inputFile.getAbsolutePath()}\"",
                                                   "--outputDir=\"${tempOutputDir.getAbsolutePath()}\""
        ], environment, Timeout)
        System.out.println("Result from operation=${result}")

        List<List<Map<String, String>>> results = ['person.json', 'title.json', 'organization.json'].collect { String filename ->
            new JsonSlurper().parse(new FileReader(new File(tempOutputDir, filename)))
        }
        new ExtractionResult(persons: results[0], titles: results[1], organizations: results[2])
    }

}
