package app

import com.vaadin.data.util.IndexedContainer

/**
 * Holds onto URLs.
 */
class UrlContainer {

    public static final Object PROPERTY_SOURCE_ID = "SourceId"
    public static final Object PROPERTY_DESCRIPTION = "Description"
    public static final Object PROPERTY_URL = "URL"
    public static final Object PROPERTY_DATE = "Date"

    public static IndexedContainer getContainer() {
        IndexedContainer container = new IndexedContainer()
        container.addContainerProperty(PROPERTY_SOURCE_ID, Long.class, null)
        container.addContainerProperty(PROPERTY_DESCRIPTION, String.class, null)
        container.addContainerProperty(PROPERTY_URL, String.class, null)
        container.addContainerProperty(PROPERTY_DATE, String.class, null)
        container
    }

}
