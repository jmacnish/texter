package app

import com.vaadin.data.util.IndexedContainer

/**
 * Simple container for titles.
 */
class TitleContainer {

    public static final Object PROPERTY_TITLE = "Name"

    static IndexedContainer getContainer() {
        def container = new IndexedContainer()
        container.addContainerProperty(PROPERTY_TITLE, String.class, null)
        container
    }

}
