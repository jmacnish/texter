package app

import com.vaadin.data.util.IndexedContainer

/**
 * Holds onto people items.
 */
class PeopleContainer {

    public static final Object PROPERTY_NAME = "Name"
    public static final Object PROPERTY_GENDER = "Gender"

    static IndexedContainer getContainer() {
        def container = new IndexedContainer()
        container.addContainerProperty(PROPERTY_NAME, String.class, null)
        container.addContainerProperty(PROPERTY_GENDER, String.class, null)
        container
    }

}
