package texter

class Source {
    String url
    String description
    Date dateCreated
    Date lastUpdated

    static constraints = {
    }
}
