package texter

class Content {
    Source source
    String content
    Date dateCreated
    Date lastUpdated

    static fetchMode = [
            source: 'eager'
    ]

    static constraints = {
        content(size:0..2147483646)
    }

}
