import texter.Content
import texter.SampleData
import texter.Source

/**
 * Initialize texter with some simple startup data.
 */
class BootStrap {

// Normally we'd do if (Environment.current == Environment.DEVELOPMENT) but just set up bootstrap data everywhere.

    def init = { servletContext ->
        Source isHere = Source.findByUrl('http://pressroom.target.com/leadership')
        if (isHere == null) {
            Source targetDirectors = new Source(url: 'http://investors.target.com/phoenix.zhtml?c=65828&p=irol-govBoard', description: 'Target Board of Directors')
            targetDirectors.save(failOnError: true)

            Source targetLeaders = new Source(url: 'http://pressroom.target.com/leadership', description: 'Target Leadership')
            targetLeaders.save(failOnError: true)

            Content directors = new Content(source: targetDirectors, content: SampleData.TargetBoardOfDirectors)
            directors.save(failOnError: true)

            Content leaders = new Content(source: targetLeaders, content: SampleData.TargetLeadership)
            leaders.save(failOnError: true)

            Source geBoard = new Source(url: 'http://www.ge.com/investor-relations/governance/board-of-directors', description: 'General Electric Board of Directors')
            geBoard.save(failOnError: true)

            Source yahooBoard = new Source(url: 'https://investor.yahoo.net/directors.cfm', description: 'Yahoo Board of Directors')
            yahooBoard.save(failOnError: true)
        }
    }

    def destroy = {
    }

}

