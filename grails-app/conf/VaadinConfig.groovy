vaadin {

    mapping = [
            "/*": "app.TexterUI"
    ]

    // Extra mapping in case you need 'reserve' a URL and it shouldn't be mapped to e.g. /* by Vaadin
    // mappingExtras = [
    //         '/console/*'
    // ]

    productionMode = false

    // Uncomment this to enable asynchronous communication, useful for vaadin-push
    // asyncSupported = true

    // Theme isn't too pretty -- disable for now.
    //themes = ['texter']
    //sassCompile = '7.1.12'
}

environments {
    production {
        vaadin {
            productionMode = true
        }
    }
}
