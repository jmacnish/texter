package texter

import grails.transaction.Transactional

/**
 * Handles transactional work while we deal with Vaadin.
 */
@Transactional
class ContentService {

    // Normally would just do this with the domain object, but since we're in vaadin it's a little stranger.
    void updateSource(Long sourceId, String url, String description) {
        Source source = Source.get(sourceId)
        if (source != null) {
            source.url = url
            source.description = description
            source.save(failOnError: true)
        }
    }

    List<Source> findSources() {
        Source.executeQuery("FROM Source AS s ORDER BY s.dateCreated DESC")
    }

    Content findLastContent(Source source) {
        findLastContent(source.id)
    }

    Content findLastContent(Long sourceId) {
        List<Content> contents = Content.executeQuery("FROM Content AS c WHERE c.source.id = ? ORDER BY c.dateCreated DESC", [sourceId])
        if (contents.size() == 0) {
            null
        } else {
            contents.get(0)
        }
    }

    Content fetchContentFromSource(Long sourceId) {
        Source source = Source.get(sourceId)
        try {
            final String content = new URL(source.url).getText()
            Content newContent = new Content(source: source, content: content)
            newContent.save(failOnError: true)
            newContent
        } catch (Exception e) {
            null
        }
    }

    Source createNewSource() {
        Source source = new Source(url: '<Double Click to Edit>', description: '<Double Click to Edit>')
        source.save(failOnError: true)
        source
    }


}
