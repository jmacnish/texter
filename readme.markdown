texter
======

A Grails application to try out GATE text processing.  Uses 'textgate' as the driver for extracting text.

Live demo at http://wowkin.com/texter/
